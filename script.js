console.log("Good afternoon world!");

// ===============The cube of 2 is 8================
let num = 2;
const getCube = Math.pow(num,3);
console.log(`The cube of ${num} is ${getCube}.`);

// I live at 258 Washington Ave NW, California 90011
// Array destructuring.
const address = [
	258,
	"Washington Ave NW",
	"California",
	"90011"
];
const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}.`);

// Lolong was a salwater crocodile. He weighed at 1075 kgs with a measurement of 20ft 3in.  
// RIP lolong. you were scary.
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075",
	measurement: {
		feet: 20,
		inches: 3,
	},
};
console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement.feet} ft and ${animal.measurement.inches} in.`);

// Number Array and Reduce Number
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(`${number}`));
const sum = numbers.reduce((acc, cur) => acc + cur,0);
console.log(sum);


// Class Dog.
class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);

